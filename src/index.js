import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  NavLink,
} from "react-router-dom";
import Settings from './Settings';
import { types, getSnapshot } from "mobx-state-tree"

function CurrentDate(){
  var days = [
    'Вc', 'Пy', 'Вт', 'Ср', 'Чn', 'Пn', 'С,'
  ];
  var months = [
    'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'
  ];
  
  
  const [time, setTime] = useState("");
  
  useEffect(() => {
    function updateTimeText(){
      var date = new Date();
      var day = date.getDate();
      let dayName = days[date.getDay()];
      let monthName = months[date.getMonth()];
      var seconds = date.getSeconds();
      if (seconds < 10) seconds = "0" + seconds.toString(); else seconds.toString();
      var minutes = date.getMinutes();
      if (minutes < 10) minutes = "0" + minutes.toString(); else minutes.toString();
      var time = date.getHours() + ':' + 
        minutes + ':' + 
        seconds;
      setTime(`${dayName}, ${day} ${monthName}, ` + time);
    }

    updateTimeText();

    setInterval(function() {
      updateTimeText();
    }, 1000);
  }, []);

  return (<p>{time}</p>);
}

function Header() {
  const [mainPage, setMainPage] = React.useState(0);

  return (
    <div class="header">
      <div class="headerCenter">
        <div class="headerCenterLeft">
          <p class="titleLogo">Todo лист</p>
          <p class="titleDate"><CurrentDate /></p>
        </div>
        <div class="headerCenterRight">
          <NavLink className={({ isActive }) => (isActive ? 'activeLink' : 'inactiveLink')} to="/settings">
            <button>Настройки</button></NavLink>
          <NavLink className={({ isActive }) => (isActive ? 'activeLink' : 'inactiveLink')} to="/">
            <button>Список</button></NavLink>

        </div>
      </div>
    </div>
  );
}





ReactDOM.render(
  <div class="page">
  <Router>
    <Header />
    <Routes>
      <Route path="/settings" caseSensitive={false} element={<Settings />} />
      <Route path="/" caseSensitive={false} element={<App />} />
    </Routes>
  </Router> 
  </div>,
  document.getElementById('root')
);

reportWebVitals();



// mobx exp
const TodoTask = types.model({
  id: types.optional(types.integer, 0),
  name: types.optional(types.string, ""),
  status: types.optional(types.boolean, false)
})
.actions(self => ({
  add(id, name, status) {
    self.id = id;
    self.name = name;
    self.status = status;
  }
}))

const TodoList = types
  .model({
      tasks: types.map(TodoTask)
  })
  .actions(self => ({
      addTodo(id, name, status) {
          self.tasks.set(id, TodoTask.create({ id, name, status }))
      }
  }))

const todoList = TodoList.create();

todoList.addTodo(1, "Задача 1", false);
todoList.addTodo(2, "Задача 2", true);

console.clear();
console.log("Tasks:", getSnapshot(todoList))