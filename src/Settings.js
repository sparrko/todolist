import React, { useState, useEffect } from 'react';
import './App.css';

function Settings() {
    function clearCompleted() {
        var list = JSON.parse(localStorage.getItem('todo_list'));
        
        var bufList = [];
        list.forEach(item => {
            if (item.status === false) bufList.push(item); 
        });

        localStorage.setItem('todo_list', JSON.stringify(bufList));

        window.location.href="../";
    }
    function resetCashe() {
        var bufList = [
            { id:1, text:"Сделанное дело", status:true },
            { id:2, text:"Не сделанное дело", status:false }
        ];
        localStorage.setItem('todo_list', JSON.stringify(bufList));
        localStorage.setItem('todo_notFirstStart', '1');

        window.location.href="../";
        window.reload();
    }

    return (
        <div>
        <div class="main">
            <p>Настройки</p>
            <div class="mainCard">
                <div class="links">
                    <a href='#resetCashe'>Сбросить настройки по умолчанию</a>
                    <a href='#clearCompleted'>Стереть все выполненые задачи</a>
                </div>
                
            </div>
            
        </div>
        <div id="resetCashe" class="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Сброс настроек</h3>
                    <a href="#close" title="Close" class="close">×</a>
                </div>
                <div class="modal-body">    
                    <p>Вы уверены? Данное действие не обратимо!</p>
                    <a href="#close"><button>Нет</button></a>
                    <a href="#close"><button onClick={() => resetCashe()}>Да</button></a>
                </div>
                </div>
            </div>
        </div>
        <div id="clearCompleted" class="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Стереть выполненые</h3>
                    <a href="#close" title="Close" class="close">×</a>
                </div>
                <div class="modal-body">  
                    <p>Вы уверены? Данное действие не обратимо!</p>
                    <a href="#close"><button>Нет</button></a>
                    <a href="#close"><button onClick={() => clearCompleted()}>Да</button></a>
                </div>
                </div>
            </div>
        </div>
    </div>
    );
}
  
export default Settings;