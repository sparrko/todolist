import React, { useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";
import './App.css';
import Settings from './Settings';
import { Formik } from 'formik';


function Row(props) {
  var {
    id,
    text,
    status,

    itemDelete,
    itemChangeStatus
  } = props;

  return (
    <li class="subCard" key={id}>
      <table>
        <tr>
          <td>
            <input type="checkbox" defaultChecked={status} onChange={
              (e) => { itemChangeStatus(e, id); }}></input>
          </td>
          <td>
            <p>{text}</p>
          </td>
          <td>
            <button onClick={() => itemDelete(props.id)}>Удалить</button>
          </td>
        </tr>
      </table>
    </li>
  );
}

function List(props) {
  const {
    list,
    setList
  } = props;

  function itemDelete(id) {
    var bufList = [...list.filter((item) => item.id != id)];

    localStorage.setItem('todo_list', JSON.stringify(bufList));
    setList(bufList);
  }

  function itemChangeStatus(e, id) {
    var bufList = list.map(x => x);
    for (const item of bufList) {
      if (item.id === id) {
        item.status = e.target.checked; break;
      }
    }

    localStorage.setItem('todo_list', JSON.stringify(bufList));
    setList(bufList);
  }

  return (
    <ul class="mainCard">
      { list.map(item => { return <Row 
            id={item.id} 
            text={item.text} 
            status={item.status} 

            itemDelete={itemDelete}
            itemChangeStatus={itemChangeStatus}
            /> }) }
    </ul>
  );
}

function Form(props) {
  const {
    list,
    setList
  } = props;

  // function handleSubmit(e){
  //   e.preventDefault();

  //   if (e.target[0].value == null || /^\s*$/.test(e.target[0].value)) {
  //     alert("Сначало введите текст! С пустым полем не прокатит.");
  //   } 
  //   else{
  //     var newId = Math.max.apply(Math, list.map(function(o) { return o.id; }));
  //     newId = (newId == '-Infinity') ? 1 : newId + 1;

  //     var bufList = [...list, { id: newId, text: e.target[0].value, status: false }];
  //     setList(bufList);
      
  //     localStorage.setItem('todo_list', JSON.stringify(bufList));

  //     e.target[0].value = "";
  //   }
  // }

  return (
    <div class="mainCard">
      {/* <form onSubmit={handleSubmit}>
        <input type="text" name="todo_name"
          placeholder="Введите текст задачи..."
        ></input>
        <button type="submit">Добавить</button>
      </form> */}

      <Formik
       initialValues={{ text: '' }}
       validate={values => {
        const errors = {};
        if(/^\s*$/.test(values.text)){
          errors.text = "Пробелы не в счет :)";
        }
        if (!values.text) {
          errors.text = 'Необходимо заполнить';
        } 
        
        return errors;
      }}
       onSubmit={(values, { resetForm }) => {
          var newId = Math.max.apply(Math, list.map(function(o) { return o.id; }));
          newId = (newId == '-Infinity') ? 1 : newId + 1;

          var bufList = [...list, { id: newId, text: values.text, status: false }];
          setList(bufList);
          
          localStorage.setItem('todo_list', JSON.stringify(bufList));
          resetForm();
      }}
     >
       {({
         values,
         errors,
         touched,
         handleChange,
         handleBlur,
         handleSubmit,
         isSubmitting,
         /* and other goodies */
       }) => (
         <form onSubmit={handleSubmit}>
            <input 
              type="text" 
              name="text"
              placeholder={errors.text && touched.text && errors.text}
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.text}
            ></input>
            
            <button type="submit">Добавить</button>
         </form>
       )}
     </Formik>
    </div>
  );
}



function Todo() { 
  const [list, setList] = useState([]);

  useEffect(() => {
    if (localStorage.getItem('todo_notFirstStart') == null) {
      var bufList = [
        { id:1, text:"Сделанное дело", status:true },
        { id:2, text:"Не сделанное дело", status:false }
      ];
      localStorage.setItem('todo_list', JSON.stringify(bufList));
      setList(bufList);
      localStorage.setItem('todo_notFirstStart', '1');
    }
    else{
      setList(JSON.parse(localStorage.getItem('todo_list')));
    }
  }, []);

  return (

      <div class="main">
        <p>Создание задачи</p>
        <Form list={list} setList={setList} />
        <div class="filters">
          <p>Список задач</p>
        </div>
        <List list={list} setList={setList} />
      </div>
    
  );
}

function App() {
  return (
    <Todo />
  );
}

export default App;
